google.maps.event.addDomListener(window, 'load', init);

function init() {
  var mapOptions = {
    zoom: 16,
    scrollwheel: false,
    center: new google.maps.LatLng(50.075558, 19.909234),
    styles: [{
      "featureType": "all",
      "elementType": "labels.text.fill",
      "stylers": [{
        "saturation": 36
      }, {
        "color": "#419d8c"
      }, {
        "lightness": "-43"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "visibility": "on"
      }, {
        "color": "#419d8c"
      }, {
        "lightness": "-70"
      }]
    }, {
      "featureType": "all",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-17"
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-66"
      }, {
        "weight": 1.2
      }]
    }, {
      "featureType": "administrative",
      "elementType": "labels.text",
      "stylers": [{
        "lightness": "-63"
      }]
    }, {
      "featureType": "administrative",
      "elementType": "labels.text.fill",
      "stylers": [{
        "lightness": "43"
      }, {
        "color": "#419d8c"
      }]
    }, {
      "featureType": "administrative",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-69"
      }]
    }, {
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-51"
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-54"
      }]
    }, {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#419d8c"
      }]
    }, {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-23"
      }]
    }, {
      "featureType": "road",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-66"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-62"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-70"
      }, {
        "weight": 0.2
      }]
    }, {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-60"
      }]
    }, {
      "featureType": "road.local",
      "elementType": "geometry",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-54"
      }]
    }, {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-59"
      }]
    }, {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-61"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-45"
      }]
    }, {
      "featureType": "water",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "color": "#419d8c"
      }, {
        "lightness": "-62"
      }]
    }]
  };
  var mapElement = document.getElementById('map');
  var map = new google.maps.Map(mapElement, mapOptions);
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(50.075558, 19.909234),
    map: map,
    scrollwheel: false,
    title: 'Wydział Inżynierii Elektrycznej i Komputerowej'
  });
  var infowindow = new google.maps.InfoWindow({
    content: '<div id="iw-container">' +
      '<div class="iw-content">' +
      '<div class="iw-subTitle">History</div>' +
      '<div class="img"><img src="assets/images/wieik.jpg" alt="Porcelain Factory of Vista Alegre" width="100%"></div>' +
      '<div class="iw-title">Wydział Inżynierii Elektrycznej i Komputerowej</div>' +
      '<p>Founded in 1824, the Porcelain Factory of Vista Alegre was the first industrial unit dedicated to porcelain production in Portugal. For the foundation and success of this risky industrial development was crucial the spirit of persistence of its founder, José Ferreira Pinto Basto. Leading figure in Portuguese society of the nineteenth century farm owner, daring dealer, wisely incorporated the liberal ideas of the century, having become "the first example of free enterprise" in Portugal.</p>' +
      '<div class="iw-subTitle">Contacts</div>' +
      '<p>VISTA ALEGRE ATLANTIS, SA<br>3830-292 Ílhavo - Portugal<br>' +
      '<br>Phone. +351 234 320 600<br>e-mail: geral@vaa.pt<br>www: www.myvistaalegre.com</p>' +
      '</div>' +
      '<div class="iw-bottom-gradient"></div>' +
      '</div>'
  });


  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map, marker);
    // alert("test");
  });
  google.maps.event.addListener(map, 'click', function(event) {
    this.setOptions({
      scrollwheel: true
    });
  });

}
