/**
	1. FIXED MENU
	2. COUNTER
  3. SCROLL TOP BUTTON
	4. SCROLL DOWN HEADER BUTTON
	5. MENU SMOOTH SCROLLING
	6. MIXIT FILTER ( FOR PORTFOLIO )
	7. MOBILE MENU CLOSE
	8 PRELOADER
	9. WOW ANIMATION
	10. MAP WHEEL LOCKER ( DEACTIVATES WITH MAP CLICK )
**/

jQuery(function($) {

    /* ----------------------------------------------------------- */
    /*  1. FIXED MENU
    /* ----------------------------------------------------------- */

    jQuery(window).bind('scroll', function() {
        if ($(window).scrollTop() > 200) {
            $('.main-navbar').addClass('navbar-fixed-top');
            $('.logo').addClass('logo-compressed');
            $('.main-nav li a').addClass('less-padding');
            $('.search-area').css('height', '44');
            $('.search-area input[type="text"]').css('top', '35%');

        } else {
            $('.main-navbar').removeClass('navbar-fixed-top');
            $('.logo').removeClass('logo-compressed');
            $('.main-nav li a').removeClass('less-padding');
            $('.search-area').css('height', '60');
            $('.search-area input[type="text"]').css('top', '11%');
        }
    });

    /* ----------------------------------------------------------- */
    /*  2. COUNTER
    /* ----------------------------------------------------------- */

    jQuery('.counter').counterUp({
        delay: 10,
        time: 1000
    });

    /* ----------------------------------------------------------- */
    /*  3. SCROLL TOP BUTTON
    /* ----------------------------------------------------------- */

    jQuery(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    jQuery('.scrollToTop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    /* ----------------------------------------------------------- */
    /*  4. SCROLL DOWN HEADER BUTTON
    /* ----------------------------------------------------------- */

    jQuery('.scrollDown').click(function() {
        $('html, body').animate({
            scrollTop: "600px"
        }, 800);
        return false;
    });

    /* ----------------------------------------------------------- */
    /*  5. MENU SMOOTH SCROLLING
    /* ----------------------------------------------------------- */

    var lastId,
        topMenu = $(".main-nav"),
        topMenuHeight = topMenu.outerHeight() + 80,
        menuItems = topMenu.find("a"),
        scrollItems = menuItems.map(function() {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });

    menuItems.click(function(e) {
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight;
        jQuery('html, body').stop().animate({
            scrollTop: offsetTop
        }, 1500);
        e.preventDefault();
    });

    jQuery(window).scroll(function() {
        var fromTop = $(this).scrollTop() + topMenuHeight;

        var cur = scrollItems.map(function() {
            if ($(this).offset().top < fromTop)
                return this;
        });
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            menuItems
                .parent().removeClass("active")
                .end().filter("[href=#" + id + "]").parent().addClass("active");
        }
    })

    /* ----------------------------------------------------------- */
    /*  6. MIXIT FILTER ( FOR SPEAKERS )
    /* ----------------------------------------------------------- */

    jQuery(function() {
        $('#mixit-container').mixItUp();
    });

    /* ----------------------------------------------------------- */
    /*  7. MOBILE MENU CLOSE
    /* ----------------------------------------------------------- */

    jQuery('.navbar-nav').on('click', 'li a', function() {
        $('.in').collapse('hide');
    });

    /* ----------------------------------------------------------- */
    /*  8. PRELOADER
    /* ----------------------------------------------------------- */

    jQuery(window).load(function() {
        $('.loader').fadeOut();
        $('#preloader').delay(100).fadeOut('slow');
        $('body').delay(100).css({
            'overflow': 'visible'
        });
    })

    /* ----------------------------------------------------------- */
    /*  9. WOW ANIMATION
    /* ----------------------------------------------------------- */

    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();
});

/* ----------------------------------------------------------- */
/*  10. MAP WHEEL LOCKER ( DEACTIVATES WITH MAP CLICK )
/* ----------------------------------------------------------- */

$(document).ready(function() {
    $('#map_canvas1').addClass('scrolloff');
    $('#google-map').on('click', function() {
        $('#map_canvas1').removeClass('scrolloff');
    });

    $("#map_canvas1").mouseleave(function() {
        $('#map_canvas1').addClass('scrolloff');
    });
});
