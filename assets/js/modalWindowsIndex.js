// Modal windows variables
var androidModal = document.getElementById('android-modal');
var springModal = document.getElementById('spring-modal');
var tddModal = document.getElementById('tdd-modal');
var ccModal = document.getElementById('cc-modal');
var wjModal = document.getElementById('wj-modal');
var ibModal = document.getElementById('ib-modal');

//Activation buttons variables
var androidBtn = document.getElementById("android-btn");
var springBtn = document.getElementById("spring-btn");
var tddBtn = document.getElementById("tdd-btn");
var ccBtn = document.getElementById("cc-btn");
var wjBtn = document.getElementById("wj-btn");
var ibBtn = document.getElementById("ib-btn");

//Close buttons
var span1 = document.getElementsByClassName("close")[0];
var span2 = document.getElementsByClassName("close")[1];
var span3 = document.getElementsByClassName("close")[2];
var span4 = document.getElementsByClassName("close")[3];
var span5 = document.getElementsByClassName("close")[4];
var span6 = document.getElementsByClassName("close")[5];

//Displaying
androidBtn.onclick = function() {
  androidModal.style.display = "block";
}
springBtn.onclick = function() {
  springModal.style.display = "block";
}
tddBtn.onclick = function() {
  tddModal.style.display = "block";
}
 ccBtn.onclick = function() {
   ccModal.style.display = "block";
 }
  wjBtn.onclick = function() {
   wjModal.style.display = "block";
 }
  ibBtn.onclick = function() {
   ibModal.style.display = "block";
 }

//Close button
span1.onclick = function() {
  androidModal.style.display = "none";
}
span2.onclick = function() {
  springModal.style.display = "none";
}
span3.onclick = function() {
  tddModal.style.display = "none";
}
span4.onclick = function() {
  ccModal.style.display = "none";
}
span5.onclick = function() {
  wjModal.style.display = "none";
}
span6.onclick = function() {
  ibModal.style.display = "none";
}

//Outside window click close
window.onclick = function(event) {
  if (event.target == androidModal) {
    androidModal.style.display = "none";
  } else if (event.target == springModal) {
    springModal.style.display = "none";
  } else if (event.target == tddModal) {
    tddModal.style.display = "none";
  }
   else if (event.target == ccModal) {
     ccModal.style.display = "none";
   }
      else if (event.target == wjModal) {
     wjModal.style.display = "none";
   } 
    else if (event.target == ibModal) {
     ibModal.style.display = "none";
   }
}
