<?php
$your_name = htmlspecialchars($_POST["your_name"]);
$email = htmlspecialchars($_POST["email"]);
$message = htmlspecialchars($_POST["message"]);
$myemail = "mocarnytydzien@gmail.com";
$tema="Wiadomośc ze strony";

$your_name = check_input($_POST["your_name"], "Wpisz imie!");
$email = check_input($_POST["email"], "Wpisz e-mail!");
$message = check_input($_POST["message"], "Zapomniałeś o treści!");

if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email)) {
  show_error("<br /> E-mail nie jest prawidłowy");
}

$message_to_myemail = "Hello!
Imie: $your_name
E-mail: $email
Msg: $message
";

$from  = "From: $your_name <$email> \r\n Reply-To: $email \r\n";
mail($myemail, $tema, $message_to_myemail, $from);
?>
<style>
body,html {
  overflow-x: hidden;
}

body {
  font-family: 'Raleway', sans-serif;
  font-size: 16px;
  overflow-x: hidden !important;
  color: #000;
  text-align: center;;
}

ul {
  padding: 0;
  margin: 0;
  list-style: none;
}

a {
  text-decoration: none;
  color: #fff;
  padding: 20px 30px;
  background: #FE5D26;
  border-radius: 20px;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
  -ms-transition: all 0.5s;
  -o-transition: all 0.5s;
  transition: all 0.5s;
}

a:hover {
  text-decoration: none;
  color: #fff;
  padding: 20px 30px;
  background: #002726;
  border-radius: 20px;
}

a:focus {
  outline: none;
  text-decoration: none;
}

h1,h2,h3,h4,h5,h6 {
  font-family: 'Raleway', sans-serif;
}

h1 {
  color: #000;
  font-size: 30px;
  font-weight: 700;
  margin: 100px 0 50px 0;
}
</style>
  <h1>Wiadomść była wysłana.</h1>
  <p><a href="/">Główna strona</a></p>
<?php
function check_input($data, $problem = "") {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  if ($problem && strlen($data) == 0){
      show_error($problem);
}

return $data;
}
?>
